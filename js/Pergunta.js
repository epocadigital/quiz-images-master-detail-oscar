var respostaCerta;
var objDadosPergunta;

$(function (){
	//assigno os eventos aos radiobuttons
	/*$("#stage1").find("input.rbResposta").click(function (evento){
		var respostaSelecionada = event.target.id.substr(-1);
		avaliarResposta (respostaSelecionada);
		$(".rbResposta").attr('disabled', 'disabled');
	});*/
	$(".tdRespostas").addClass ("simClickEvents");
	$(".tdRespostas").click (function (evento){
		//console.log ($(this));
		var respostaSelecionada = evento.target.id.substr(-1);
		avaliarResposta (respostaSelecionada);
		$(".tdRespostas").removeClass("simClickEvents").addClass ("noClickEvents");
		$(this).find(".imgResposta").attr("src", "images/radioButtonOn.gif");
		//console.log ($(this).find(".imgResposta"));
	})


})

function initPergunta (_objDadosPergunta){
	
	//////////////////////////////////////////////////////////////////////////////////////
	//INICIALIZO TODO

	respostaCerta = false;
/*
	table rbRespostas
	tr rbResposta0, 1 y 2
	td rbResposta0, 1 y 2
	td txtRespota0, 1 y 2
*/
	//habilito os radiobuttons
	$(".labelResposta").removeClass("respostaCerta").removeClass("respostaErrada");
	$(".rbResposta").removeClass("respostaCerta").removeClass("respostaErrada");
	//$(".rbResposta").attr("checked", false);
	$(".imgResposta").attr("src", "images/radioButtonOff.gif");
	
	//show numero pergunta
	$("#numeroPergunta").text("Pergunta " + (cursor + 1) + " de " + qPerguntas);

	//deshabilito botão continuar
	$("input#continuar").attr("disabled", "disabled").css({ opacity: 0.2 });

	//apago el titulo del filme
	$("#filmeNome").text("");
	
	//habilito os radiobuttons
	//$(".rbResposta").removeAttr("disabled");
	$(".tdRespostas").removeClass("noClickEvents").addClass ("simClickEvents");
	//-----------------




	
	//reset texto avaliação resposta
	$("#showResultadoResposta").removeClass("respostaCerta").removeClass("respostaErrada").text ("");

	//////////////////////////////////////////////////////////////////////////////////////

	//assigno a imagem do detalhe
	$("#imgDetail-Grande").attr("src", "images/" + _objDadosPergunta["fotoDetalhe"]);

	//assigno os Labels das perguntas
	$("#stage1").find(".labelResposta").each(function (i){
		document.getElementById(this.id).innerHTML = _objDadosPergunta["resp" + (i+1)];
	});

	//o nome do filme, vazio
	document.getElementById("filmeNome").innerHTML = "Ao cartaz de qual filme corresponde a imagem da esquerda?";
	//o comentário, preenchido em qualquer caso
	document.getElementById("filmeComentario").innerHTML = _objDadosPergunta["concorre"];

	objDadosPergunta = _objDadosPergunta;

	
};

function avaliarResposta (respostaSelecionada){

	if (respostaSelecionada == objDadosPergunta["respostaCerta"]){
		respostaCerta = true;
		score++;
	}

	if (respostaCerta){
		//teñir de verde la respuesta
		$("#labelResposta" + respostaSelecionada).addClass("respostaCerta");
		$("#showResultadoResposta").addClass("respostaCerta").text("CERTO");

	}else{
		//teñir de rojo la respuesta dada
		$("#labelResposta" + respostaSelecionada).addClass("respostaErrada");
		//y de verde la respuesta correcta
		$("#labelResposta" + objDadosPergunta["respostaCerta"]).addClass("respostaCerta");
		$("#showResultadoResposta").addClass("respostaErrada").text ("ERRADO");
	}

	$("#filmeNome").text (objDadosPergunta["filme"]);

	var fotoGde = objDadosPergunta["fotoGrande"];
	$("#imgDetail-Grande").fadeOut('slow', function() {
		$(this).attr("src", "images/" + fotoGde).fadeIn('fast', function (){
    		setTimeout(function(){
    			$("input#continuar").removeAttr("disabled").css({ opacity: 1 });
    		},1000);
    		
    	});
  	});
	
	objDadosPergunta = null;
}

//stage 2 - final
function showResultadoScore (){
	$("#resultadoScore").text (score);
}