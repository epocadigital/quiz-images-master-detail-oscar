(function($) {
  var cache = [];
  // Arguments are image paths relative to the current page.
  $.preLoadImages = function() {
    var args_len = arguments.length;
    for (var i = args_len; i--;) {
      var cacheImage = document.createElement('img');
      cacheImage.src = arguments[i];
      cache.push(cacheImage);
    }
  }
})(jQuery);

jQuery.preLoadImages(

"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/Amor_det.jpg", 
"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/Amor.jpg", 
"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/AsAventurasDePi_det.jpg", 
"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/AsAventurasDePi.jpg", 
"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/btnContinuar.png", 
"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/btnIniciar.png", 
"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/btnOutrosTestes.png", 
"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/btnRefazer.png",
"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/DjangoLivre_det.jpg", 
"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/DjangoLivre.jpg", 
"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/imgAbre_cartaz.jpeg", 
"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/imgAbre_cartaz.jpg",
"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/imgFecha_cartaz.jpg", 
"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/imgGeral_cartaz.jpg", 
"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/IndomavelSonhadora_det.jpg", 
"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/IndomavelSonhadora.jpg",
"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/Lincoln_det.jpg", 
"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/Lincoln.jpg", 
"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/Logo_quiz.png", 
"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/OHobbit_det.jpg",
"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/OHobbit.jpg", 
"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/OsMiseraveis_det.jpg", 
"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/OsMiseraveis.jpg", 
"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/OVoo_det.jpg",
"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/OVoo.jpg", 
"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/radioButtonOff.gif", 
"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/radioButtonOn.gif",
"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/Skyfall_det.jpg", 
"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/Skyfall.jpg", 
"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/Valente_det.jpg", 
"http://s3.amazonaws.com/cdn.infografiaepoca.com.br/770quizOscarCartazes/images/Valente.jpg"
);
